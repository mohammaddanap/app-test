import {Component, OnInit,AfterViewInit, ViewChild} from '@angular/core';
import {ConfigService} from "../config/config.service";
import {MessageService} from "../config/message.service";
import {NgxSpinnerService} from "ngx-spinner";
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {catchError, map, startWith, switchMap} from "rxjs/operators";
import {merge, of as observableOf} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../models/User";
import { ViewportScroller } from '@angular/common';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements  AfterViewInit{
  form: FormGroup = new FormGroup({});
  user:User;
  imgUser;
  avatar;
  userId:string='';
  firstName: string = '';
  lastName: string = '';
  type:string='user';
  editActive:boolean=false;
  registerAt:string='';
  company:string='';
  displayedColumns: string[] = ['name', 'gender', 'email', 'phone','company','age','type','setting'];
  dataSource = new MatTableDataSource<USER>([]);

  resultsLength = 0;
  isLoadingResults = true;
  isRateLimitReached = false;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  ngAfterViewInit() {
    this.spinner.show()
    this.dataSource.paginator = this.paginator;
    var my_this = this;
    merge(this.paginator.page)
      .pipe(
        startWith({}),
        switchMap(() => {

          return this.service.GET_USERS(20,(this.paginator.pageIndex))
        }),
        map(data => {
          // Flip flag to show that loading has finished.
          console.log(data)
          this.spinner.hide();
          this.isRateLimitReached = false;
          this.resultsLength = data?.count;
          return data?.users;
        }),
        catchError(() => {
          this.spinner.hide()
          // Catch if the GitHub API has reached its rate limit. Return empty data.
          this.isRateLimitReached = true;
          return observableOf([]);
        })
      ).subscribe(data => this.dataSource = data);
  }
  currentCount:number=0;
  totalCount:number=0;
  users = [];
  pageCount:Pages[];
  page: number = 0;
  constructor(private service: ConfigService,
              private msg: MessageService,
              private spinner: NgxSpinnerService,
              private fb: FormBuilder,
              public dialog: MatDialog) {
    this.pageCount=[];
    this.user = new User();
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      lastName: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.email]],
      mobileNumber: ['', [Validators.pattern("^[0-9]*$")]],
      companyName: ['', [Validators.minLength(1)]],
      Age: ['', [Validators.pattern("^[0-9]*$"), Validators.min(1), Validators.max(200)]],
      Address: ['', [Validators.minLength(4)]],
      Gender: ['male', Validators.required],
      Type: ['user', Validators.required],
      registerAt: ['']
    });
  }
  removeUser(id) {
    this.spinner.show();
    this.service.REMOVE_USER(id).subscribe(result => {
      this.spinner.hide();
      if (result.status === 'ok') {
        this.dataSource.paginator = this.paginator;
        var my_this = this;
        merge(this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              my_this.spinner.show()
              return this.service.GET_USERS(20,(this.paginator.pageIndex))
            }),
            map(data => {
              // Flip flag to show that loading has finished.
              console.log(data)
              this.spinner.hide();
              this.isRateLimitReached = false;
              this.resultsLength = data?.count;
              return data?.users;
            }),
            catchError(() => {
              my_this.spinner.hide()
              // Catch if the GitHub API has reached its rate limit. Return empty data.
              this.isRateLimitReached = true;
              return observableOf([]);
            })
          ).subscribe(data => this.dataSource = data);
        this.msg.success('Remove User Success');
      }
    }, error => {
      this.spinner.hide();
      this.msg.e(error);
    })
  }
  removeUser_row() {
    this.spinner.show();
    this.service.REMOVE_USER(this.userId).subscribe(result => {
      this.spinner.hide();
      if (result.status === 'ok') {
        this.dataSource.paginator = this.paginator;
        var my_this = this;
        merge(this.paginator.page)
          .pipe(
            startWith({}),
            switchMap(() => {
              my_this.spinner.show()
              return this.service.GET_USERS(20,(this.paginator.pageIndex))
            }),
            map(data => {
              // Flip flag to show that loading has finished.
              console.log(data)
              this.spinner.hide();
              this.isRateLimitReached = false;
              this.resultsLength = data?.count;
              return data?.users;
            }),
            catchError(() => {
              my_this.spinner.hide()
              // Catch if the GitHub API has reached its rate limit. Return empty data.
              this.isRateLimitReached = true;
              return observableOf([]);
            })
          ).subscribe(data => this.dataSource = data);
        this.msg.success('Remove User Success');
        this.resetForm();

      }
    }, error => {
      this.spinner.hide();
      this.msg.e(error);
    })
  }
  createIdRandom(length){
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  submit() {
    return this.form.controls;
  }
  get f() {
    return this.form.controls;
  }
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imgUser = reader.result as string;
        this.avatar = reader.result;
      };

    }
  }
  getDate(){
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    let _Date = mm + '/' + dd + '/' + yyyy;
    return _Date;

  }
  RegisterUser(){
    this.spinner.show();
    if (this.checkValidate()){
      this.user={
        id:this.createIdRandom(20),
        name:this.form.get('name').value,
        email:this.form.get('email').value,
        mobile:this.form.get('mobileNumber').value,
        lastName:this.form.get('lastName').value,
        company:this.form.get('companyName').value,
        age:this.form.get('Age').value,
        address:this.form.get('Address').value,
        gender:this.form.get('Gender').value,
        type:this.form.get('Type').value,
        registerAt:this.getDate(),
        photo:this.avatar==undefined?(''):(this.avatar)
      }
      this.service.REGISTER_USER(this.user).subscribe(result=>{
        if (result.status==='ok'){
          this.spinner.hide();
          this.dataSource.paginator = this.paginator;
          var my_this = this;
          merge(this.paginator.page)
            .pipe(
              startWith({}),
              switchMap(() => {
                setTimeout(() => my_this.spinner.show(), 0);
                return this.service.GET_USERS(20,(this.paginator.pageIndex))
              }),
              map(data => {
                // Flip flag to show that loading has finished.
                console.log(data)
                this.spinner.hide();
                this.isRateLimitReached = false;
                this.resultsLength = data?.count;
                return data?.users;
              }),
              catchError(() => {
                this.spinner.hide()
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                this.isRateLimitReached = true;
                return observableOf([]);
              })
            ).subscribe(data => this.dataSource = data);
          this.msg.success('User Registered.');
          this.resetForm();
          this.editActive=false;

        }
      },error => {
        this.spinner.hide();
        this.msg.e(error);
      })
    }
    else {
      this.msg.e("Invalid Data!");
      this.spinner.hide();
    }

  }
  checkValidate(){
    if (this.form.controls.Address.status==='VALID'&&
      this.form.controls.Age.status==='VALID'&&
      this.form.controls.Gender.status==='VALID'&&
      this.form.controls.Type.status==='VALID'&&
      this.form.controls.companyName.status==='VALID'&&
      this.form.controls.email.status==='VALID'&&
      this.form.controls.lastName.status==='VALID'&&
      this.form.controls.mobileNumber.status==='VALID'&&
      this.form.controls.name.status==='VALID'
    ){
      return true;
    }
    else {
      return false;
    }

  }
  resetForm(){
    this.form.reset();
    this.userId='';
    this.editActive=false;
    this.avatar = '';
    this.imgUser = '../../assets/images/avatar.svg';
    this.form.controls['Gender'].setValue('male');
    this.form.controls['Type'].setValue('user');
  }
  getUser(data){
    this.editActive=true;
    this.form.controls['name'].setValue(data?.name)
    this.form.controls['lastName'].setValue(data?.last_name);
    this.form.controls['email'].setValue(data?.email);
    this.form.controls['mobileNumber'].setValue(data?.mobile);
    this.form.controls['companyName'].setValue(data?.company);
    this.form.controls['Age'].setValue(data?.age);
    this.form.controls['Address'].setValue(data?.address);
    this.form.controls['Gender'].setValue(data?.gender);
    this.form.controls['Type'].setValue(data?.type);
    this.registerAt = data?.register_at;
    this.type = data?.type;
    if (data?.photo){
      this.avatar = data?.photo;
      this.imgUser = data?.photo
    }
    else{
      this.avatar = '';
      this.imgUser = '../../assets/images/avatar.svg';
    }
    this.userId=data?.id;
  }
  UpdateProfile(){
    this.spinner.show();
    if (this.checkValidate()){
      this.user={
        id:this.userId,
        name:this.form.get('name').value,
        email:this.form.get('email').value,
        mobile:this.form.get('mobileNumber').value,
        lastName:this.form.get('lastName').value,
        company:this.form.get('companyName').value,
        age:this.form.get('Age').value,
        address:this.form.get('Address').value,
        gender:this.form.get('Gender').value,
        type:this.form.get('Type').value,
        registerAt:this.form.get('registerAt').value,
        photo:this.avatar
      }
      this.service.UPDATE_USER(this.user,this.userId).subscribe(result=>{
        this.spinner.hide();
        if (result.status==='ok'){
          this.dataSource.paginator = this.paginator;
          var my_this = this;
          merge(this.paginator.page)
            .pipe(
              startWith({}),
              switchMap(() => {
                setTimeout(() => my_this.spinner.show(), 0);
                return this.service.GET_USERS(20,(this.paginator.pageIndex))
              }),
              map(data => {
                // Flip flag to show that loading has finished.
                console.log(data)
                this.spinner.hide();
                this.isRateLimitReached = false;
                this.resultsLength = data?.count;
                return data?.users;
              }),
              catchError(() => {
                this.spinner.hide()
                // Catch if the GitHub API has reached its rate limit. Return empty data.
                this.isRateLimitReached = true;
                return observableOf([]);
              })
            ).subscribe(data => this.dataSource = data);
          this.msg.success('User Updated.');
          this.resetForm();
          this.editActive=false;
        }
      },error => {
        this.spinner.hide();
        console.log(error);
      })
    }
    else {
      this.msg.e('Invalid Data!');
      this.spinner.hide();
    }

  }
}

export interface Pages {
  number:number;
  isActive:boolean;
}
export interface USER {
  address: string;
  age: number;
  company: string;
  email: string;
  gender: string;
  id: string;
  last_name: string;
  mobile: string;
  name: string;
  photo: string;
  register_at:string;
  type: string;
}
