import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListComponent} from "./list/list.component";
import {RegisterFormComponent} from "./register-form/register-form.component";
import {EditFormComponent} from "./edit-form/edit-form.component";

const routes: Routes = [
  {path:'',redirectTo:'/List',pathMatch:'full'},
  {path:'List',component:ListComponent},
  {path:'Register',component:RegisterFormComponent},
  {path:'Edit/:id',component:EditFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ anchorScrolling: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
