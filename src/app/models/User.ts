export class User{
  id:string;
  name:string;
  lastName:string;
  mobile:string;
  email?:string;
  company?:string;
  photo?:string;
  address?:string;
  gender?:string;
  type:string;
  registerAt:string;
  age?:number;

}
