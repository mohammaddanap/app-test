import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ConfigService} from "../config/config.service";
import {ActivatedRoute, Router} from "@angular/router";
import {NgxSpinnerService} from "ngx-spinner";
import {MessageService} from "../config/message.service";
import {User} from "../models/User";

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  form: FormGroup = new FormGroup({});
  user:User;
  imgUser;
  avatar;
  firstName: string = '';
  lastName: string = '';
  type:string='user';

  constructor(private service:ConfigService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private msg:MessageService) {
    this.user = new User();
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      lastName: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.email]],
      mobileNumber: ['', [Validators.pattern("^[0-9]*$")]],
      companyName: ['', [Validators.minLength(1)]],
      Age: ['', [Validators.pattern("^[0-9]*$"), Validators.min(1), Validators.max(200)]],
      Address: ['', [Validators.minLength(4)]],
      Gender: ['male', Validators.required],
      Type: ['user', Validators.required]
    });
  }
  ngOnInit(): void {

  }
  createIdRandom(length){
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }
  submit() {
    return this.form.controls;
  }
  get f() {
    return this.form.controls;
  }
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imgUser = reader.result as string;
        this.avatar = reader.result;
      };

    }
  }
  getDate(){
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    let _Date = mm + '/' + dd + '/' + yyyy;
    return _Date;

  }
  RegisterUser(){
    this.spinner.show();
    if (this.checkValidate()){
      this.user={
        id:this.createIdRandom(20),
        name:this.form.get('name').value,
        email:this.form.get('email').value,
        mobile:this.form.get('mobileNumber').value,
        lastName:this.form.get('lastName').value,
        company:this.form.get('companyName').value,
        age:this.form.get('Age').value,
        address:this.form.get('Address').value,
        gender:this.form.get('Gender').value,
        type:this.form.get('Type').value,
        registerAt:this.getDate(),
        photo:this.avatar==undefined?(''):(this.avatar)
      }
      this.service.REGISTER_USER(this.user).subscribe(result=>{
        if (result.status==='ok'){
          this.spinner.hide();
          this.msg.success('User Registered.');
          this.router.navigate(['/List']);
        }
      },error => {
        this.spinner.hide();
        this.msg.e(error);
      })
    }
    else {
      this.msg.e("Invalid Data!");
      this.spinner.hide();
    }

  }
  checkValidate(){
    if (this.form.controls.Address.status==='VALID'&&
      this.form.controls.Age.status==='VALID'&&
      this.form.controls.Gender.status==='VALID'&&
      this.form.controls.Type.status==='VALID'&&
      this.form.controls.companyName.status==='VALID'&&
      this.form.controls.email.status==='VALID'&&
      this.form.controls.lastName.status==='VALID'&&
      this.form.controls.mobileNumber.status==='VALID'&&
      this.form.controls.name.status==='VALID'
    ){
      return true;
    }
    else {
      return false;
    }

  }
}
