import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {User} from "../models/User";

export const API = environment.API_URL;
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }),
  // withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http:HttpClient) { }

  GET_USERS(count:number,offset:number){
    return this.http.get(`${API}/user?count=${count}&offset=${offset>0?count:offset}`)
      .pipe(catchError(handleError<any>('Get List Users')));
  }
  GET_PROFILE_USERS(id:string){
    return this.http.get(`${API}/user/${id}`)
      .pipe(catchError(handleError<any>('Get List Users')));
  }
  REGISTER_USER(user:User){
    return this.http.post(`${API}/user`, {user:user},httpOptions)
      .pipe(catchError(handleError<any>('Register User')));
  }
  UPDATE_USER(user:User,id:string){

    return this.http.put(`${API}/user/${id}`,{user:user},httpOptions)
      .pipe(catchError(handleError<any>('UPDATE USER')));
  }
  REMOVE_USER(id){
    return this.http.delete(`${API}/user/${id}`)
      .pipe(catchError(handleError<any>('Destroy User')));
  }
}

function handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // TODO: better job of transforming error for user consumption
    // UtilitesService.log(`${operation} failed: ${error.message}`);

    return throwError(error.error);
  };
}
