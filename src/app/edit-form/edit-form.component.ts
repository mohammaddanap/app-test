import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../models/User";
import {ActivatedRoute, Router} from "@angular/router";
import {ConfigService} from "../config/config.service";
import {NgxSpinnerService} from "ngx-spinner";
import {MessageService} from "../config/message.service";

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})
export class EditFormComponent implements OnInit {
  private sub: any;
  myThis = this;
  user:User;
  form: FormGroup = new FormGroup({});
  imgUser;
  avatar;
  id:string;
  firstName: string = '';
  lastName: string = '';
  company:string='';
  registerAt:string='';
  type:string='';

  constructor(private service:ConfigService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private spinner: NgxSpinnerService,
              private msg:MessageService) {
    this.user = new User();
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      lastName: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.email]],
      mobileNumber: ['', [Validators.pattern("^[0-9]*$")]],
      companyName: ['', [Validators.minLength(1)]],
      Age: ['', [Validators.pattern("^[0-9]*$"), Validators.min(1), Validators.max(200)]],
      Address: ['', [Validators.minLength(4)]],
      Gender: ['male', Validators.required],
      Type: ['user', Validators.required],
      registerAt: [''],
    })
  }
  get f() {
    return this.form.controls;
  }
  ngOnInit(): void {
    this.sub = this.route.params.subscribe(params=>{
      this.myThis.id = params['id']
      this.myThis.GetProfile(this.myThis.id);
    })
  }
  submit() {
    console.log(this.avatar)
    return this.form.controls;
  }
  onFileChange(event) {
    const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imgUser = reader.result as string;
        this.avatar = reader.result;
      };

    }
  }
  GetProfile(id){
    this.spinner.show();
    this.service.GET_PROFILE_USERS(id).subscribe(result=>{
      this.spinner.hide();
      this.form.controls['name'].setValue(result.user?.name)
      this.form.controls['lastName'].setValue(result.user?.last_name);
      this.form.controls['email'].setValue(result.user?.email);
      this.form.controls['mobileNumber'].setValue(result.user?.mobile);
      this.form.controls['companyName'].setValue(result.user?.company);
      this.form.controls['Age'].setValue(result.user?.age);
      this.form.controls['Address'].setValue(result.user?.address);
      this.form.controls['Gender'].setValue(result.user?.gender);
      this.form.controls['Type'].setValue(result.user?.type);
      this.registerAt = result.user?.register_at;
      this.type = result.user?.type;
      if (result.user?.photo){
        this.avatar = result.user?.photo;
        this.imgUser = result.user?.photo
      }
      else{
        this.avatar = '';
        this.imgUser = '../../assets/images/avatar.svg';
      }
    },error => {
      this.spinner.hide();
    })
  }
  UpdateProfile(){
    this.spinner.show();
    if (this.checkValidate()){
      this.user={
        id:this.id,
        name:this.form.get('name').value,
        email:this.form.get('email').value,
        mobile:this.form.get('mobileNumber').value,
        lastName:this.form.get('lastName').value,
        company:this.form.get('companyName').value,
        age:this.form.get('Age').value,
        address:this.form.get('Address').value,
        gender:this.form.get('Gender').value,
        type:this.form.get('Type').value,
        registerAt:this.form.get('registerAt').value,
        photo:this.avatar
      }
      this.service.UPDATE_USER(this.user,this.id).subscribe(result=>{
        this.spinner.hide();
        if (result.status==='ok'){
          this.msg.success('User Updated.');
          this.router.navigate(['/List']);
        }
      },error => {
        this.spinner.hide();
        console.log(error);
      })
    }
    else {
      this.msg.e('Invalid Data!');
      this.spinner.hide();
    }

  }
  checkValidate(){
    if (this.form.controls.Address.status==='VALID'&&
      this.form.controls.Age.status==='VALID'&&
      this.form.controls.Gender.status==='VALID'&&
      this.form.controls.Type.status==='VALID'&&
      this.form.controls.companyName.status==='VALID'&&
      this.form.controls.email.status==='VALID'&&
      this.form.controls.lastName.status==='VALID'&&
      this.form.controls.mobileNumber.status==='VALID'&&
      this.form.controls.name.status==='VALID'
    ){
      return true;
    }
    else {
      return false;
    }

  }
  removeUser() {
    this.spinner.show();
    this.service.REMOVE_USER(this.id).subscribe(result => {
      this.spinner.hide();
      if (result.status === 'ok') {
        this.router.navigate(['/List']);
        this.msg.success('Remove User Success');
      }
    }, error => {
      this.spinner.hide();
      this.msg.e(error);
    })
  }
}
